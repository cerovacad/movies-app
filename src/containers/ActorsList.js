import React from "react";
import ActorItem from "../components/ActorItem";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import { fetchActorsAsync } from "../actions/actorsActions";

const styles = {
  card: {
    display: "flex",
    flexDirection: "column"
  }
};

class Actors extends React.Component {
  componentDidMount() {
    this.props.fetchActorsAsync()
  }
  render() {
    const { classes, actors } = this.props
    return (
      <div className={classes.card}>
        {actors.map(actor => (
          <ActorItem key={actor.id} actorInfo={actor} />
        ))}
      </div>
    );
  }
}

Actors.propTypes = {
  classes: PropTypes.object.isRequired,
  actors: PropTypes.array.isRequired,
};

const MapStateToProps = state => {
  return {
    actors: state.actors
  };
};

export default connect(
  MapStateToProps,
  { fetchActorsAsync }
)(withStyles(styles)(Actors));
