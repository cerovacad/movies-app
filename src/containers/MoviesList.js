import React from "react";
import MovieItem from "../components/MovieItem";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import { PropTypes } from "prop-types";
import { fetchMoviesAsync } from "../actions/moviesActions";

const styles = {
  flex: {
    display: "flex",
    flexDirection: "column"
  }
};
class Movies extends React.Component {
  componentDidMount() {
    this.props.fetchMoviesAsync()
  }
  render() {
    const { classes, movies } = this.props
    return (
      <div className={classes.flex}>
        {movies.map(movie => (
          <MovieItem key={movie.id} movieInfo={movie} />
        ))}
      </div>
    );
  }
}

Movies.propTypes = {
  classes: PropTypes.object.isRequired,
  movies: PropTypes.array.isRequired
}

const MapStateToProps = state => {
  return {
    movies: state.movies
  };
};

export default connect(
  MapStateToProps,
  { fetchMoviesAsync }
)(withStyles(styles)(Movies));
