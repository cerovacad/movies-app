import { handleActions } from "redux-actions";
import { fetchItemById } from "../actions/itemActions";

const defState = {};

export default handleActions(
  {
    [fetchItemById]: (state, { payload } ) => {
      return payload;
    }
  },
  defState
);