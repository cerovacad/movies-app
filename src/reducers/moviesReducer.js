import { handleActions } from "redux-actions";
import { fetchMovies } from "../actions/moviesActions";

const defState = [];

export default handleActions(
  {
    [fetchMovies]: (state, { payload }) => {
      return payload;
    }
  },
  defState
);
