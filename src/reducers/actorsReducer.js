import { handleActions } from "redux-actions";
import { fetchActors } from "../actions/actorsActions";

const defState = [];

export default handleActions(
  {
    [fetchActors]: (state, { payload }) => {
      return payload;
    }
  },
  defState
);
