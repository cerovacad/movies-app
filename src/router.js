import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Nav from "./components/Nav";
import ActorsList from "./containers/ActorsList";
import MoviesList from "./containers/MoviesList";
import NotFound from "./components/NotFound";
import MovieFull from "./components/MovieFull";
import ActorFull from "./components/ActorFull";
import "./index.css";
import SignUp from "./components/SignUp";
import Login from "./components/Login";
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";

const PrivateRoute = ({ component, isAuth, ...rest }) => (
  <Route
    {...rest}
    component={isAuth ? component : () => <Redirect to="/" />}
  />
);

const App = ({ isAuth }) => (
  <Router>
    <div>
      <div>
        <Nav />
      </div>
      <div className="main">
        <Switch>
          <Route path="/" component={Login} exact />
          <Route path="/signup" component={SignUp} exact />
          <PrivateRoute
            isAuth={isAuth}
            path="/actors"
            component={ActorsList}
            exact
          />
          <PrivateRoute
            isAuth={isAuth}
            path="/movies"
            component={MoviesList}
            exact
          />
          <PrivateRoute
            isAuth={isAuth}
            path="/movies/:id"
            component={MovieFull}
            exact
          />
          <PrivateRoute
            isAuth={isAuth}
            path="/actors/:id"
            component={ActorFull}
            exact
          />
          <Route component={NotFound} />
        </Switch>
      </div>
    </div>
  </Router>
);

const mapStateToProps = state => {
  return {
    isAuth: state.isAuth
  };
};

export default connect(mapStateToProps)(App);
