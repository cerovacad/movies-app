import React from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import FavoriteIcon from "@material-ui/icons/Favorite";
import ShareIcon from "@material-ui/icons/Share";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import { dashToSpace } from "../utils/helpers";
import NotFound from "./NotFound";
import { fetchItemByIdAsync } from "../actions/itemActions";

const styles = theme => ({
  card: {
    width: 450,
    margin: "1rem auto"
  },
  media: {
    height: 350,
    paddingTop: "56.25%" // 16:9
  },
  like: {
    color: "#E57373"
  },
  actions: {
    display: "flex"
  }
});

class RecipeReviewCard extends React.Component {
  state = {
    like: false
  };

  handleLike = () => {
    this.setState({ like: !this.state.like });
  };

  componentDidMount() {
    this.props.fetchItemByIdAsync(this.props.match.url);
  }

  render() {
    const { classes } = this.props;
    const { actor } = this.props;
    return (
      <div>
        {actor ? (
          <Card className={classes.card}>
            <CardHeader
              action={
                <IconButton>
                  <MoreVertIcon />
                </IconButton>
              }
              title={`${actor.first_name} ${actor.last_name}`}
              subheader={`Born: ${dashToSpace(actor.born)}`}
            />
            <CardMedia
              className={classes.media}
              image="/img/actors/actor.jpg"
              title={`${actor.born} poster`}
            />
            <CardContent>
              <Typography>{actor.biography}</Typography>
            </CardContent>
            <CardActions className={classes.actions} disableActionSpacing>
              <IconButton aria-label="Add to favorites">
                <FavoriteIcon
                  className={this.state.like ? classes.like : ""}
                  onClick={this.handleLike}
                />
              </IconButton>
              <IconButton aria-label="Share">
                <ShareIcon />
              </IconButton>
            </CardActions>
          </Card>
        ) : (
          <NotFound />
        )}
      </div>
    );
  }
}

RecipeReviewCard.propTypes = {
  classes: PropTypes.object.isRequired,
  actor: PropTypes.object.isRequired
};

const mapStateToProps = state => {
  return {
    actor: state.itemId
  };
};

export default connect(
  mapStateToProps,
  { fetchItemByIdAsync }
)(withStyles(styles)(RecipeReviewCard));
