import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Input from "@material-ui/core/Input";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import { Link } from "react-router-dom";
import { auth } from "../actions/authActions";
import { connect } from "react-redux";

const styles = theme => ({
  root: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
    margin: "2rem auto",
    width: 350,
    height: 200
  },
  container: {
    display: "flex",
    flexDirection: "column"
  },
  input: {
    margin: theme.spacing.unit
  },
  button: {
    margin: theme.spacing.unit
  },
  link: {
    color: "inherit",
    textAlign: "center",
    marginTop: "1rem"
  },
  err: {
    textAlign: "center",
    padding: "0.35rem",
    color: "#E57373"
  }
});

class Inputs extends React.Component {
  state = {
    email: "",
    password: "",
    err: null
  };
  handleEmailChange = e => {
    this.setState({ email: e.target.value });
  };
  handlePasswordChange = e => {
    this.setState({ password: e.target.value });
  };
  handleLogin = () => {
    if (this.state.password.length <= 3) {
      this.setState({
        email: "",
        password: "",
        err: "Incorrect email and password. Try again."
      });
    } else {
      this.props.auth(`${this.state.email}${this.state.password}`);
      this.props.history.push("/movies");
    }
  };
  render() {
    const { classes } = this.props;
    const { email, password, err } = this.state;
    return (
      <Paper className={classes.root} elevation={1}>
        <div className={classes.container}>
          <Input
            placeholder="Email"
            className={classes.input}
            error={Boolean(err)}
            inputProps={{
              "aria-label": "Description"
            }}
            value={email}
            onChange={this.handleEmailChange}
          />
          <Input
            placeholder="Password"
            className={classes.input}
            error={Boolean(err)}
            inputProps={{
              "aria-label": "Description"
            }}
            type="password"
            value={password}
            onChange={this.handlePasswordChange}
          />
          {err && <div className={classes.err}>{err}</div>}
          <Button
            onClick={this.handleLogin}
            variant="contained"
            className={classes.button}
          >
            Login
          </Button>
          <Link className={classes.link} to="/signup">
            Don't have an account?
          </Link>
        </div>
      </Paper>
    );
  }
}

Inputs.propTypes = {
  classes: PropTypes.object.isRequired
};

export default connect(
  null,
  { auth }
)(withStyles(styles)(Inputs));
