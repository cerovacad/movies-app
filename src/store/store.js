import { createStore, applyMiddleware, combineReducers } from "redux";
import moviesReducer from "../reducers/moviesReducer";
import actorsReducer from "../reducers/actorsReducer";
import itemIdReducer from "../reducers/itemIdReducer";
import authReducer from "../reducers/authReducer";
import logger from "redux-logger";
import thunk from "redux-thunk";

const rootReducer = combineReducers({
  movies: moviesReducer,
  actors: actorsReducer,
  itemId: itemIdReducer,
  isAuth: authReducer,
});

export default createStore(rootReducer, applyMiddleware(thunk, logger));
