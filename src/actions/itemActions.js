import { createAction } from "redux-actions";
import axios from "axios";
import { andrijana } from "../utils/consts";

export const fetchItemById = createAction("FETCH_MOVIE_BY_ID")

export const fetchItemByIdAsync = (id) => {
  return dispatch => {
    axios
      .get(`${andrijana}${id}`)
      .then(res => {
        dispatch(fetchItemById(res.data))
      })
      .catch(err => {
        console.log(err);
      });    
  }
}