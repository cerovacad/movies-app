import { createAction } from "redux-actions";
import axios from "axios";
import { andrijana } from "../utils/consts";

export const fetchActors = createAction("FETCH_ACTORS");

export const fetchActorsAsync = () => {
  return dispatch => {
    axios
      .get(`${andrijana}/actors/`)
      .then(res => {
        dispatch(fetchActors(res.data));
      })
      .catch(err => {
        console.log(err);
      });
  };
};
