import { createAction } from "redux-actions";
import axios from "axios";
import { andrijana } from "../utils/consts";

export const fetchMovies = createAction("FETCH_MOVIES");
export const fetchMovieById = createAction("FETCH_MOVIE_BY_ID")

export const fetchMoviesAsync = () => {
  return dispatch => {
    axios
      .get(`${andrijana}/movies`)
      .then(res => {
        dispatch(fetchMovies(res.data));
      })
      .catch(function(error) {
        console.log(error);
      });
  };
};


